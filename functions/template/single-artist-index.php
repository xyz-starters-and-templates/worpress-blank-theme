<div class="infoWrap">
	<div class="infoWrap">		
			<?php 
			$artist_artwork_args = array(
			    'connected_type' => 'artist_artwork',
			    'connected_items' => intval(get_the_ID()),        
			    //'nopaging' => true,
			    'orderby'=>'post_date',
			    'order'=>'asc',
			    'posts_per_page'=>-1
			    
			);
			global $meta_box_artwork;
			$artwork_query = new WP_Query($artist_artwork_args);
			$artwork_count=0;
			if($artwork_query->have_posts()):
			while($artwork_query->have_posts()): $artwork_query->the_post(); ;
			$thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'artwork-square');
			$il = wp_get_attachment_image_src( get_post_thumbnail_id(), 'artwork-thumbnail');
			foreach ($meta_box_artwork['fields'] as $custom_field){
				${$custom_field['id']}=get_post_meta($post->ID, $custom_field['id'], true);
			}
			if ($artwork_count==0):
			?>
		<div class="infoLeft">
			<div id="artMimgBox">
						<a href="#"><img src="<?php echo $il[0];?>" /></a>
			</div>
		</div>
		<div class="infoRight">
			<div id="artWorksInfoBox">
				<strong class="artWorksTitle"><?php the_title();?></strong>
				<span class="artWorksMsg">
					<?php echo urldecode($tk_dimension);?><br/>
					<?php echo urldecode($tk_material);?><br/>
					<?php echo urldecode($tk_code);?><br/>
					<?php echo urldecode($tk_year);?>
					<?php echo urldecode($tk_exhibition);?><br/>
				</span><span class="artWorksState"><?php echo urldecode($tk_stock);?><br/></span>
			</div>
			<div id="artWorksList">
			<?php endif;?>						
				<a href="#" value="<?php echo $artwork_count;?>"  class="workMove"><img src="<?php echo $thumbnail_image_url[0];?>" /></a>
				<a href="#" class="workUp" value="<?php echo $artwork_count;?>" rel="artWroki"></a>
			<?php $artwork_count++;endwhile;endif;
			$artwork_query->rewind_posts();?>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
var JsArtWorks=[
<?php if($artwork_query->have_posts()):
while($artwork_query->have_posts()): $artwork_query->the_post(); $artwork_count++;
foreach ($meta_box_artwork['fields'] as $custom_field){
	${$custom_field['id']}=get_post_meta($post->ID, $custom_field['id'], true);
}
$i = wp_get_attachment_image_src( get_post_thumbnail_id(), 'artwork-thumbnail');
$il = wp_get_attachment_image_src( get_post_thumbnail_id(), 'artwork-inner');
?>
{"i":"<?php echo $i[0];?>","il":"<?php echo $il[0];?>","t":"<?php echo urldecode(get_the_title());?>","d":"<?php echo urldecode(str_replace('"','\"',$tk_dimension));?>","m":"<?php echo urldecode($tk_material);?>","c":"<?php echo urldecode($tk_code);?>","y":"<?php echo urldecode($tk_year);?>","e":"<?php echo urldecode($tk_exhibition);?>","s":"<?php echo urldecode($tk_stock);?>"},
<?php endwhile;endif;?>
];
</script>
