<div class="infoWrap">
	<div class="infoLeft">
		<div class="imgBoxInfo">
			<?php $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'artist-inner');?>
			
			<img src="<?php echo $thumbnail_image_url[0];?>" alt="<?php the_title();?>" width="<?php echo $thumbnail_image_url[1];?>" height="<?php echo $thumbnail_image_url[2];?>"/>				
			<span id="pBgInfo"></span>
		</div>
	</div>
	<div class="infoRight">
		<?php the_content();?>
	</div>
</div>