<?php get_header(); ?>
<div class="content" id="content_press">
	
	<?php 
	

	$artist_press_args = array(
	    'connected_type' => 'artist_press',
	    'connected_items' => intval(get_the_ID()),        
	    //'nopaging' => true,
	    //'orderby'=>'menu_oder',
	    //'order'=>'ASC',
	    'posts_per_page'=>-1
	    
	);
	global $meta_box_press;
	$press_query = new WP_Query($artist_press_args);
	
	if($press_query->have_posts()):
	while($press_query->have_posts()): $press_query->the_post(); ;				
		foreach ($meta_box_press['fields'] as $custom_field){
			${$custom_field['id']}=get_post_meta($post->ID, $custom_field['id'], true);
		}
	?>
	<div class="infoWrap">
		<div class="infoLeft">
			<div class="imgBoxInfo">
                <a href="<?php the_permalink();?>">
                	<?php $thumbnail_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-pagination');?>
                    <img src="<?php echo $thumbnail_image_url[0];?>" width="<?php echo $thumbnail_image_url[1];?>" height="<?php echo $thumbnail_image_url[2];?>"/> 
                </a>
				<span id="pBgInfo"></span>
			</div>
		</div>
		<div class="infoRight">
			<strong class="infoTitle"><?php the_title();?></strong>
			<span class="infoTime"><?php the_time('F j, Y');?></span>
			<div class="infoContent">
			<?php the_excerpt();?>
			<a href="<?php the_permalink();?>">more</a>
			</div>
		</div>
	</div>
	<?php endwhile;endif;wp_reset_query();?>
</div>
<?php get_footer(); ?>
